/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.filah.webserviceteste;

import com.filah.webserviceteste.modelos.EBook;
import com.filah.webserviceteste.modelos.Livro;
import java.util.ArrayList;
import java.util.List;
import com.filah.webserviceteste.modelos.Autor;


/**
 *
 * @author Ricci
 */
public class LivroBuilder {
    
    List<Autor> autores;
    
    public LivroBuilder(){
        autores =  new ArrayList<Autor>();
        autores.add(new Autor("Denis"));
        autores.add(new Autor("Rodrigo"));
        
    }
    
    public List<Livro> getLivros(){
        Livro livro1 = new Livro();
        Livro livro2 = new Livro();
        Livro livro3 = new Livro();
        
        livro1.setId(1);
        livro2.setId(2);
        livro3.setId(3);
        
        livro1.setDescricao("livro1");
        livro2.setDescricao("livro2");
        livro3.setDescricao("livro3");
        
        livro1.setTitulo("titulo1");
        livro2.setTitulo("titulo2");
        livro3.setTitulo("titulo3");
        
        
        
        livro1.setAutor(autores);
        livro2.setAutor(autores);
        livro3.setAutor(autores);        
        
        List<Livro> livros = new ArrayList<Livro>();
        
        livros.add(livro1);
        livros.add(livro2);
        livros.add(livro3);
        
        return livros;    
    }
    
    public List<EBook> getEbooks(){
        EBook ebook1 = new EBook();
        EBook ebook2 = new EBook();
        EBook ebook3 = new EBook();
        
        ebook1.setId(1);
        ebook2.setId(2);
        ebook3.setId(3);
        
        ebook1.setDescricao("ebook1");
        ebook2.setDescricao("ebook2");
        ebook3.setDescricao("ebook3");
        
        ebook1.setTitulo("titulo1");
        ebook2.setTitulo("titulo2");
        ebook3.setTitulo("titulo3");
        
        
        ebook1.setAutor(autores);
        ebook2.setAutor(autores);
        ebook3.setAutor(autores);        
        
        List<EBook> ebooks = new ArrayList<EBook>();
        
        ebooks.add(ebook1);
        ebooks.add(ebook2);
        ebooks.add(ebook3);
        
        return ebooks;    
    
    }
}
