/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.filah.webserviceteste;

import java.util.ArrayList;
import java.util.List;
import com.filah.webserviceteste.modelos.Autor;

/**
 *
 * @author Ricci
 */
public class AutorBuilder {
    public List<Autor> getAutores(){
        Autor autor1 = new Autor("Denis");
        Autor autor2 = new Autor("Rodrigo");
        Autor autor3 = new Autor("Thiago");
        
        List<Autor> autores = new ArrayList<Autor>();
        autores.add(autor1);
        autores.add(autor2);
        autores.add(autor3);
        
        return autores;
    }
}
