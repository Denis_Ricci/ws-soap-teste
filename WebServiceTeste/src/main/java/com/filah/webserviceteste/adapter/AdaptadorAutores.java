package com.filah.webserviceteste.adapter;


import javax.xml.bind.annotation.adapters.XmlAdapter;
import com.filah.webserviceteste.modelos.Autor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ricci
 */
public class AdaptadorAutores extends XmlAdapter<String, Autor> {

    @Override
    public String marshal(Autor autor) throws Exception {
        return autor.getNome();
    }

    @Override
    public Autor unmarshal(String autor) throws Exception {
        return new Autor(autor);
    }
}
