/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.filah.webserviceteste.servicos;

import com.filah.webserviceteste.AutorBuilder;
import java.util.List;
import com.filah.webserviceteste.modelos.Autor;
import javax.jws.WebService;

/**
 *
 * @author Ricci
 */
@WebService
public class AutoresService {
    
    public List<Autor> listaAutores(){
        return new AutorBuilder().getAutores();
    }
    
}
