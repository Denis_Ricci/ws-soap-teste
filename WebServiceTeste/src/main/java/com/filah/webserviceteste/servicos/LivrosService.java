/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.filah.webserviceteste.servicos;

import com.filah.webserviceteste.LivroBuilder;
import com.filah.webserviceteste.modelos.EBook;
import com.filah.webserviceteste.modelos.Livro;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 *
 * @author Ricci
 */
@WebService
public class LivrosService {

    /**
     * @param args the command line arguments
     */
        @WebResult(name="livro")
        public List<Livro> listarLivros() {		
		return new LivroBuilder().getLivros();
	}
        
        @RequestWrapper(className = "com.filah.webserviceteste.servicos.jaxws.ListaLivroIndice", localName = "listaLivrosIndice")
        @ResponseWrapper(className = "com.filah.webserviceteste.servicos.jaxws.ListaLivroIndiceResponse", localName = "unicoLivro")
        @WebMethod(operationName="listarLivroIndice")
        public Livro listarLivros(int index) {		
		return new LivroBuilder().getLivros().get(index);
	}
        
        public void criaLivro(@WebParam(name="livro") Livro livro){
            System.out.println(livro.getTitulo());
        }
        
        public List<EBook> listarEbooks(){
            return new LivroBuilder().getEbooks();
        }
    
//    public static void main(String[] args) {
//        Endpoint.publish("http://localhost:8090/livros", new LivrosService());
//        System.out.println("Serviço inicializado");                
//    }
    
}
