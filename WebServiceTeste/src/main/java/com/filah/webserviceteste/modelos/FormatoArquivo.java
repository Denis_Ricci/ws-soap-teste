/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.filah.webserviceteste.modelos;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Ricci
 */
@XmlEnum
@XmlType(name = "formato")
public enum FormatoArquivo {
    @XmlEnumValue("pdf")
    PDF, 
    @XmlEnumValue("mobi")
    MOBI, 
    @XmlEnumValue("epub")
    EPUB;
}
